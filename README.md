# MatZip

Python Selenium, BeautifulSoup 패키지, re 모듈, tkinter 모듈 사용

## 기능

- **내 근처 맛집 검색**
    - 예) "치킨" 입력

        → 치킨, 통닭, 닭 등의 문자열이 포함된 주변 식당 리스트업

    - 지역 이름 함께 검색 가능

- **식당을 선택하면 평점 표시**
    - 네이버 별점

- **홍보 의심 알림**
    - 후기 게시글에서 특정 패턴이 있는 경우 해당 문장을 표시하며 홍보 의심 알림 띄우기
    - 예) [지역 이름] + 맛집 + [상호명]

- **진실한 후기 검색**
    - 홍보글이 아닌 후기 게시글 검색(부정적인 리뷰 위주)
    - "내돈내산", "비추천" 등의 키워드 검색

- **메뉴(사이드 메뉴) 검색**
    - 예) 나는 꼭 치킨과 함께 치즈볼을 먹어야겠다.

        → 메뉴 검색에 "치즈볼"을 검색하면 메뉴에 치즈볼이 있는 식당 리스트업

## 역할 분담

- 김민경: 정규 표현식 작성 및 UI 제작
- 김수헌: 정규 표현식 작성 및 크롤러 구현

## 개발 환경

- 언어: Python
- IDE: Visual Studio Code

---